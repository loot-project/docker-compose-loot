# Docker Compose stack for a dev environment of Loot application

## Requirements

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Install and run the stack

### Run a local proxy

If you run few projects in your computer, install a proxy with docker listening the port 80.

```
docker run -d --net proxy --ip 172.32.0.2 -p 80:80 \
			-v /var/run/docker.sock:/tmp/docker.sock:ro \
			--name dev.http-proxy jwilder/nginx-proxy
```

We have set up the ip address to manage easier the local ```/etc/hosts``` file.

### Install the loot docker stack 

```
git clone git@gitlab.com:loot-project/docker-compose-loot.git docker
cd docker
cp .env.dist .env
cp docker-compose.yml.dist docker-compose.yml
```

### Run the stack

Add the domain ```loot.local``` and ```www.loot.local``` to the local ```/etc/hosts``` file with the ip address of the proxy

```
sudo bash -c 'echo "172.32.0.2 lootest.local" >> /etc/hosts'
sudo bash -c 'echo "172.32.0.2 www.lootest.local" >> /etc/hosts'
```

Create the app directory next to the docker directory

```
mkdir loot-app
```

And run docker compose images

```
./bin/compose-ubuntu build
./bin/compose-ubuntu up
```