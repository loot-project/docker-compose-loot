
MYSQL_USER="mysql"
MYSQL_GROUP="mysql"
if [ ! -f /etc/${WORKER_ID} ]; then
	OLDUID=$(id -u ${MYSQL_USER})
	usermod -s /bin/bash ${MYSQL_USER}
	usermod -u ${WORKER_ID} ${MYSQL_USER}   
	groupmod -g ${WORKER_ID} ${MYSQL_GROUP}
	find / -user ${OLDUID} -exec chown -h ${WORKER_ID} {} \;
	find / -group ${OLDUID} -exec chgrp -h ${WORKER_ID} {} \;
	usermod -g ${WORKER_ID} ${MYSQL_USER}
	touch /etc/${WORKER_ID}
fi
